## Godot_Skeleton

### Description

Framework pour Godot Engine 3 (et suivants), regroupant les fonctionnalités utiles à un jeu vidéo.

### Objectifs

Aucun.

### Mouvements et actions du joueur

Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.
Quitter le jeu: clic sur la croix ou touche escape.

### Interactions

Aucun.

### Copyrights

Codé en GDscript en utilisant le moteur de jeu Godot Engine.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)

=================================================

### Description

Base Framework for Godot Engine 3 (and more), gathering the useful features for a video game.

### Goals

None.

### Movements and actions of the player

Move the player: ZQSD keys or the arrows on the keyboard.
Exit the game: click on the cross or escape key.

### Interactions

None.

### Copyrights

Coded in GDscript using the Godot Engine game engine.
Developed using mainly the integrated code editor and/or Visual Studio Code.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)
