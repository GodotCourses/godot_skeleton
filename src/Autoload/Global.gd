# Autoload Global
# Peut stocker des variables globales, telles que les informations du joueur
# Peut s’occuper des changements de scènes et des transitions
# ...

# https://docs.godotengine.org/fr/latest/getting_started/workflow/best_practices/scene_organization.html
# https://docs.godotengine.org/fr/latest/getting_started/step_by_step/singletons_autoload.html#doc-singletons-autoload

extends Node

var current_scene: Node

# load a scene securely
# usage:
# func _on_Button_pressed():
# 	Global.goto_scene("res://Scene2.tscn")
func goto_scene(path_to_new_scene):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.
	
	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:
	
	call_deferred("_deferred_goto_scene", path_to_new_scene)
	


# Internal called by goto_scene
# defered loading of a scene
func _deferred_goto_scene(path_to_new_scene):
	# It is now safe to remove the current scene
	current_scene.free()
	
	var s = ResourceLoader.load(path_to_new_scene)
	
	current_scene = s.instance()
	
	get_tree().get_root().add_child(current_scene)
	
	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)
